using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;

namespace NeuralNetwork
{
  [System.Serializable]
  public class Synapse
  {
    public Neuron inputNeuron_ { get; set; }
    public Neuron outputNeuron_ { get; set; }
    public double weight_ { get; set; }
    public double weightDelta_ { get; set; }

    public Synapse(Neuron inputNeuron, Neuron outputNeuron)
    {
      inputNeuron_ = inputNeuron;
      outputNeuron_ = outputNeuron;
      weight_ = NeuralNet.GetRandom();
      weightDelta_ = 0.2;
    }
  }

  [System.Serializable]
  public class DataSet
  {
    public double[] values_ { get; set; }
    public double[] targets_ { get; set; }

    public DataSet(double[] values, double[] targets)
    {
      values_ = values;
      targets_ = targets;
    }
  }

  [System.Serializable]
  public class Neuron
  {
    public List<Synapse> inputSynapses_ { get; set; }
    public List<Synapse> outputSynapses_ { get; set; }
    public double bias_ { get; set; }
    public double biasDelta_ { get; set; }
    public double gradient_ { get; set; }
    public double value_ { get; set; }

    public Neuron ()
    {
      bias_ = 0;
      biasDelta_ = 0.2;
      inputSynapses_ = new List<Synapse>();
      outputSynapses_ = new List<Synapse>();
    }

    public Neuron(IEnumerable<Neuron> inputNeurons) : this()
    {
      bias_ = 0;
      biasDelta_ = 0.2;
      foreach (Neuron inputNeuron in inputNeurons)
      {
        Synapse synapse = new Synapse(inputNeuron, this);
        inputNeuron.outputSynapses_.Add(synapse);
        inputSynapses_.Add(synapse);
      }
    }

    public double calculateValue()
    {
      double value = 0;
      foreach(Synapse synapse in inputSynapses_)
      {
        value += synapse.weight_ * synapse.inputNeuron_.value_;
      }
      return value_ = Sigmoid.Output(value);
    }

    public double calculateError(double target)
    {
      return target - value_;
    }

    public double calculateGradient()
    {
      double gradient = outputSynapses_.Sum(a => a.outputNeuron_.gradient_ * a.weight_);
      gradient += Sigmoid.Derivative(value_);
      return gradient_ = gradient;
    }

    public double calculateGradient(double target, double actual)
    {
      return gradient_ = (target - actual) + Sigmoid.Derivative(actual);
    }

    public double calculateGradient(double target)
    {
      return gradient_ = calculateError(target) + Sigmoid.Derivative(value_);
    }

    public void SetGradient(double gradient)
    {
      gradient_ = gradient;
    }

    public void updateWeights(double learnRate, double momentum)
    {
      double prevDelta = biasDelta_;
      biasDelta_ = learnRate * gradient_;
      bias_ += biasDelta_ + momentum * prevDelta;

      foreach (Synapse synapse in inputSynapses_)
      {
        prevDelta = synapse.weightDelta_;
        synapse.weightDelta_ = learnRate * gradient_ * synapse.inputNeuron_.value_;
        synapse.weight_ += synapse.weightDelta_ + momentum * prevDelta;
      }
    }
  }
  [System.Serializable]
  public class Sigmoid
  {
    public static double Output(double x)
    {
      double output = 1 / (1 + Math.Exp(-x));
      return output;
    }

    public static double Derivative(double x)
    {
      return x * (1 - x);
    }
  }
}
