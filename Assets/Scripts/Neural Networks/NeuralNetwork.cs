using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;

namespace NeuralNetwork
{
  [System.Serializable]
  public class NeuralNet
  {
    public double learnRate_ { get; set; }
    public double momentum_ { get; set; }
    public List<Neuron> inputLayer_ { get; set; }
    public List<List<Neuron>> hiddenLayers_ { get; set; }
    public List<Neuron> outputLayer_ { get; set; }

    public static readonly System.Random Random = new System.Random();

    public NeuralNet(int inputSize, int hiddenSize, int outputSize, int hiddenLayers, double learnRate, double momentum)
    {
      learnRate_ = learnRate;
      momentum_ = momentum;

      inputLayer_ = new List<Neuron>();
      hiddenLayers_ = new List<List<Neuron>>();
      outputLayer_ = new List<Neuron>();

      for (int i = 0; i < inputSize; i++)
      {
        inputLayer_.Add(new Neuron());
      }

      for (int i = 0; i < hiddenLayers; i++)
      {
        hiddenLayers_.Add(new List<Neuron>());
        for (int j = 0; j < hiddenSize; j++)
        {
          hiddenLayers_[i].Add(new Neuron(i==0?inputLayer_:hiddenLayers_[i-1]));
        }
      }

      for (int i = 0; i < outputSize; i++)
      {
        outputLayer_.Add(new Neuron(hiddenLayers_[hiddenLayers - 1]));
      }
    }

    public void ForwardPropogate(double[] inputs)
    {
      int i = 0;
      inputLayer_.ForEach(a => a.value_ = inputs[i++]);
      foreach (List<Neuron> layer in hiddenLayers_)
      {
        layer.ForEach(a => a.calculateValue());
      }
      outputLayer_.ForEach(a => a.calculateValue());
    }

    public void BackPropogate(double[] targets)
    {
      int i = 0;
      outputLayer_.ForEach(a => a.calculateGradient(targets[i++]));
      foreach (List<Neuron> layer in hiddenLayers_.AsEnumerable<List<Neuron>>().Reverse())
      {
        layer.ForEach(a => a.calculateGradient());
        layer.ForEach(a => a.updateWeights(learnRate_, momentum_));
      }
      outputLayer_.ForEach(a => a.updateWeights(learnRate_, momentum_));
    }

    public void BackPropogateAction(double target, double actual, int id)
    {
      for (int i = 0; i < outputLayer_.Count; i++)
      {
        if (i == id)
        {
          outputLayer_[i].calculateGradient(target, actual);
        }
      }

      foreach (List<Neuron> layer in hiddenLayers_.AsEnumerable<List<Neuron>>().Reverse())
      {
        layer.ForEach(a => a.calculateGradient());
        layer.ForEach(a => a.updateWeights(learnRate_, momentum_));
      }

      for (int i = 0; i < outputLayer_.Count; i++)
      {
        if (i == id)
          outputLayer_[i].updateWeights(learnRate_, momentum_);
      }
    }

    public double[] Compute(double[] inputs)
    {
      ForwardPropogate(inputs);
      return outputLayer_.Select(a => a.value_).ToArray();
    }

    public double CalculateError(double[] targets_)
    {
      int i = 0;
      return outputLayer_.Sum(a => Mathf.Abs((float)a.calculateError(targets_[i++])));
    }

    public static double GetRandom()
    {
      return 2 * Random.NextDouble() - 1;
    }

    public void Train(List<DataSet> dataSets, double minimumError)
    {
      double error = 1.0;
      double numOfEpochs = 0;

      while (error > minimumError && numOfEpochs < int.MaxValue)
      {
        List<double> errors = new List<double>();
        foreach(DataSet dataSet in dataSets)
        {
          ForwardPropogate(dataSet.values_);
          BackPropogate(dataSet.targets_);
          errors.Add(CalculateError(dataSet.targets_));
        }

        error = errors.Average();
        numOfEpochs++;
      }
    }
  }
}
