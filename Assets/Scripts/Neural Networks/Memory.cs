using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using NeuralNetwork;

[System.Serializable]
public class Memory
{
  double[] initialState_;
  int actionID_;
  float predValue_;
  float reward_;
  double[] consequentState_;

  public Memory(double[] initialState, int actionID, float reward, float predValue, double[] consequentState)
  {
    initialState_ = initialState;
    actionID_ = actionID;
    predValue_ = predValue;
    reward_ = reward;
    consequentState_ = consequentState;
  }

  public int GetID()
  {
    return actionID_;
  }

  public float GetPredValue()
  {
    return predValue_;
  }

  public float GetReward()
  {
    return reward_;
  }
}
