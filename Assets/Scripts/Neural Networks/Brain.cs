using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using NeuralNetwork;

public class Brain : MonoBehaviour
{
  public string m_areaNetworkFile;
  public string m_combatNetworkFile;
  public string m_targetingNetworkFile;
  public string m_memoryDirectory;

  NeuralNet m_areaNetwork;
  NeuralNet m_combatNetwork;
  NeuralNet m_targetingNetwork;

  List<Memory> m_targetingMemories;
  List<Memory> m_combatMemories;

  void Awake()
  {
    m_areaNetwork = Serializer.Load<NeuralNet>(Application.dataPath + "/" + m_areaNetworkFile);
    m_combatNetwork = Serializer.Load<NeuralNet>(Application.dataPath + "/" + m_combatNetworkFile);
    m_targetingNetwork = Serializer.Load<NeuralNet>(Application.dataPath + "/" + m_targetingNetworkFile);

    m_targetingMemories = Serializer.Load<List<Memory>>(Application.dataPath + "/" + m_memoryDirectory + "/Targeting/TargetingMemories.mem");
    m_combatMemories = Serializer.Load<List<Memory>>(Application.dataPath + "/" + m_memoryDirectory + "/Combat/Combatmemories.mem");

    if (m_targetingMemories == null) m_targetingMemories = new List<Memory>();
    if (m_combatMemories == null) m_combatMemories = new List<Memory>();
  }

  void Update()
  {

  }

  public int GetIdealArea(double[] inputs)
  {
    double[] outputs = m_areaNetwork.Compute(inputs);
    outputs = Utilities.SoftMax(outputs);

    int areaid = 0;
    double max = 0;
    for (int i = 0; i < outputs.GetLength(0); i++)
    {
        if (outputs[i] > max)
        {
          max = outputs[i];
          areaid = i;
        }
    }

    return areaid++;
  }

  public CombatAction GetBestAction(double[] inputs, out double output)
  {
    double[] outputs = m_combatNetwork.Compute(inputs);
    outputs = Utilities.SoftMax(outputs);

    CombatAction bestAction = CombatAction.none;
    double max = 0;
    for (int i = 0; i < outputs.GetLength(0); i++)
    {
      if (outputs[i] > max)
      {
        max = outputs[i];
        bestAction = (CombatAction)i;
      }
    }

    output = max;

    return bestAction;
  }

  public int[] GetTargetingAction(double[] inputs, out double[] values)
  {
    double[] outputs = m_targetingNetwork.Compute(inputs);
    outputs = Utilities.SoftMax(outputs);

    double[] indexedOutputs = outputs;
    Array.Sort(outputs);

    List<int> IDs = new List<int>();

    for (int i = 0; i < 3; i++)
    {
      for (int j = 0; j < 3; j++)
      {
        if (outputs[i] == indexedOutputs[j]) IDs.Add(j);
      }
    }

    values = outputs;

    return IDs.ToArray();
  }

  public void CombatLearn(List<qAction> actions)
  {
    foreach (qAction action in actions)
    {
      m_combatNetwork.BackPropogateAction(action.GetReward(), action.GetPredValue(), action.GetID());
      AddCombatMemory(action.CreateMemory());
    }
  }

  public void CombatLearn(Memory memory)
  {
    m_combatNetwork.BackPropogateAction(memory.GetReward(), memory.GetPredValue(), memory.GetID());
  }

  public void AddCombatMemory(Memory memory)
  {
    m_combatMemories.Add(memory);
  }

  public void AddTargetingMemory(Memory memory)
  {
    m_targetingMemories.Add(memory);
  }

  public void TargetLearn(qAction action)
  {
    m_targetingNetwork.BackPropogateAction(action.GetReward(), action.GetPredValue(), action.GetID());
    AddTargetingMemory(action.CreateMemory());
  }

  public void TargetLearn(Memory memory)
  {
    m_targetingNetwork.BackPropogateAction(memory.GetReward(), memory.GetPredValue(), memory.GetID());
  }

  void OnDestroy()
  {
    Serializer.Save(Application.dataPath + "/" + m_combatNetworkFile, m_combatNetwork);
    Serializer.Save(Application.dataPath + "/" + m_targetingNetworkFile, m_targetingNetwork);

    Serializer.Save(Application.dataPath + "/" + m_memoryDirectory + "/Combat/CombatMemories.mem", m_combatMemories);
    Serializer.Save(Application.dataPath + "/" + m_memoryDirectory + "/Targeting/TargetingMemories.mem", m_targetingMemories);
  }
}
