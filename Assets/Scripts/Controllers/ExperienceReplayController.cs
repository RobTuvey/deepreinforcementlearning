using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ExperienceReplayController : MonoBehaviour
{
  public string m_memoryDirectory;
  public ExperienceReplayManager m_manager;
  public bool m_learn;
  public TextMesh m_text;

  Brain m_brain;
  List<Memory> m_combatMemories;
  List<Memory> m_targetingMemories;

  int m_currentCombatMemory = 0;
  int m_currentTargetingMemory = 0;
  int m_totalMemories;

  void Awake()
  {
    m_brain = gameObject.GetComponent<Brain>();

    m_text.text = "Memories: 0";

    ImportMemories();
  }

  void Update()
  {
    if(m_learn)
    {
      if (m_currentCombatMemory < m_combatMemories.Count)
      {
        m_brain.CombatLearn(m_combatMemories[m_currentCombatMemory]);

        m_currentCombatMemory++;
        m_totalMemories++;
        m_text.text = "Memories: " + m_totalMemories.ToString();
      }
      else m_currentCombatMemory = 0;

      if (m_currentTargetingMemory < m_targetingMemories.Count)
      {
        m_brain.TargetLearn(m_targetingMemories[m_currentTargetingMemory]);

        m_currentTargetingMemory++;
        m_totalMemories++;
        m_text.text = "Memories: " + m_totalMemories.ToString();
      }
      else m_currentTargetingMemory = 0;
    }
  }

  public void ImportMemories()
  {
    m_combatMemories = m_manager.ImportMemories(m_memoryDirectory + "/Combat");
    m_targetingMemories = m_manager.ImportMemories(m_memoryDirectory + "/Targeting");
  }

  public void StartLearning()
  {
    m_learn = true;
  }

  public void StopLearning()
  {
    m_learn = false;
  }
}
