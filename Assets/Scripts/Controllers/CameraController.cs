using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class CameraController : MonoBehaviour
{
  Transform m_transform;
  public Transform player_transform;
  Rigidbody m_rigidbody;

  Vector3 idealPosition;
  Vector3 idealLocalPosition;
  bool colliding;

  public float resetSpeed = 10;
  public float repulseSpeed = 1;

  void Awake()
  {
    m_transform = gameObject.GetComponent<Transform>();
    m_rigidbody = gameObject.GetComponent<Rigidbody>();
    idealPosition = m_transform.position;
    idealLocalPosition = m_transform.localPosition;
  }

  void Update()
  {
    if (!colliding)
    {
      Vector3 dir = idealLocalPosition - m_transform.localPosition;

      m_transform.position = Vector3.Lerp(m_transform.position, m_transform.position + m_transform.TransformDirection(dir), resetSpeed * Time.deltaTime);
    }
  }

  void OnTriggerEnter(Collider other)
  {
    if (other.gameObject.tag == "Wall")
    {
      Vector3 position = new Vector3(player_transform.position.x, m_transform.position.y, player_transform.position.z);
      m_transform.position = Vector3.Lerp(m_transform.position, position, repulseSpeed * Time.deltaTime);

      colliding = true;
    }
    if (other.gameObject.tag == "WalkableGround")
    {
      m_transform.position = Vector3.Lerp(m_transform.position, player_transform.position, repulseSpeed * Time.deltaTime);

      colliding = true;
    }
  }

  void OnTriggerStay(Collider other)
  {
    if (other.gameObject.tag == "Wall")
    {
      Vector3 position = new Vector3(player_transform.position.x, m_transform.position.y, player_transform.position.z);
      m_transform.position = Vector3.Lerp(m_transform.position, position, repulseSpeed * Time.deltaTime);
    }
    if (other.gameObject.tag == "WalkableGround")
    {
      m_transform.position = Vector3.Lerp(m_transform.position, player_transform.position, repulseSpeed * Time.deltaTime);
    }
  }

  void OnTriggerExit(Collider other)
  {
    if (other.gameObject.tag == "Wall" || other.gameObject.tag == "WalkableGround")
    {
      colliding = false;
    }
  }
}
