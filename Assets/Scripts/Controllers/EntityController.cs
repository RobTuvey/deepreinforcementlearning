using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.AI;

public class EntityController : MonoBehaviour
{
  public EntityID m_entityID;
  public Vector3 m_spawnPosition;
  public float m_health = 100.0f;
}
