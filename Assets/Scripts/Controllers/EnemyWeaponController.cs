using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class EnemyWeaponController : MonoBehaviour
{
    public Transform m_transform;
    public Transform p_transform;

    public bool shoot;
    float nextShoot = 0;

    void Start()
    {
      m_transform = GetComponent<Transform>();
      p_transform = m_transform.parent;
    }

    void Update()
    {
      if (shoot)
      {
        Shoot();
      }

      RaycastHit hit;
      Physics.Raycast(p_transform.position, p_transform.forward, out hit);
      m_transform.LookAt(hit.point);
    }

    public void Shoot()
    {
      if (Time.time > nextShoot)
      {
        WeaponContainer weapon = m_transform.GetChild(0).gameObject.GetComponent<WeaponContainer>();
        ParticleSystem particle = weapon.particlesystem.GetComponent<ParticleSystem>();

        nextShoot = Time.time + weapon.rateOfFire;

        GameObject bullet = Instantiate(weapon.bulletPrefab, weapon.bulletSpawn.position, weapon.bulletSpawn.rotation);
        weapon.initBullet(bullet, p_transform.gameObject.GetComponent<EntityController>().m_entityID);

        particle.Play();
      }
    }
}
