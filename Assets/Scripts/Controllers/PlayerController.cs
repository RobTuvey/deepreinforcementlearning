using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class PlayerController : EntityController
{
  Rigidbody m_rigidbody;
  public Rigidbody m_cameraRigidBody;
  public Transform m_transform;
  public Transform m_cameraTransform;
  InputManager m_input;

  Vector3 initialCameraVector;

  [SerializeField]
  float moveSpeed = 10;
  [SerializeField]
  float jumpSpeed = 10;
  [SerializeField]
  float rotationSpeed = 10;
  [SerializeField]
  float minCamAngle = -0.1f;
  [SerializeField]
  float maxCamAngle = 0.4f;

  public float rotateX;
  public float rotateY;
  public bool isAirbourne = false;

  void Awake()
  {
    m_input = GetComponent<InputManager>();
    m_rigidbody = GetComponent<Rigidbody>();
    m_transform = GetComponent<Transform>();
    m_spawnPosition = m_transform.position;

    initialCameraVector = m_cameraTransform.position - m_transform.position;
    initialCameraVector.y = 0;
  }

  void Start()
  {
    m_entityID = EntityID.player;
  }

  void Update()
  {
    updateMovement();
  }

  void updateMovement()
  {

  }

  void FixedUpdate()
  {
    Vector3 force = new Vector3(m_input.mov_xAxis * (moveSpeed * Time.deltaTime), 0,m_input.mov_zAxis * (moveSpeed * Time.deltaTime));
    force = Quaternion.Euler(0, m_transform.eulerAngles.y, 0) * force;
    m_rigidbody.velocity += force;

    m_rigidbody.velocity = new Vector3(Mathf.Clamp(m_rigidbody.velocity.x, -5.0f, 5.0f),
                                      Mathf.Clamp(m_rigidbody.velocity.y, -100.0f, 100.0f),
                                      Mathf.Clamp(m_rigidbody.velocity.z, -5.0f, 5.0f));

    rotateX = m_input.loo_yAxis * (rotationSpeed * Time.deltaTime);
    rotateY = m_input.loo_xAxis * (rotationSpeed * Time.deltaTime);

    float rotX = m_cameraTransform.localRotation.x;
    if (rotX < minCamAngle) rotX = minCamAngle;
    if (rotX > maxCamAngle) rotX = maxCamAngle;

    m_cameraTransform.localRotation = new Quaternion(rotX,
                                                m_cameraTransform.localRotation.y,
                                                m_cameraTransform.localRotation.z,
                                                m_cameraTransform.localRotation.w);

    if (rotateY < -0.5)
    {
      rotateY = rotateY;
    }

    m_cameraTransform.Rotate(Vector3.right * rotateX);

    m_transform.Rotate(m_transform.up * rotateY);

    if (m_input.button0 && !isAirbourne)
          m_rigidbody.AddForce(new Vector3(0, jumpSpeed, 0));
  }

  void LateUpdate()
  {
    m_cameraTransform.localEulerAngles = new Vector3(m_cameraTransform.localEulerAngles.x, 0, 0);
    m_cameraTransform.localPosition = Vector3.zero;
  }

  void OnTriggerEnter(Collider other)
  {
    if (other.gameObject.tag == "WalkableGround")
    {
      isAirbourne = false;
    }
  }

  void OnTriggerStay(Collider other)
  {
    if (other.gameObject.tag == "WalkableGround")
    {
      isAirbourne = false;
    }
  }

  void OnTriggerExit(Collider other)
  {
    if (other.gameObject.tag == "WalkableGround")
    {
      isAirbourne = true;
    }
  }

  void OnCollisionEnter(Collision collision)
  {
    if (collision.gameObject.tag == "Bullet")
    {
      BulletController bc = collision.gameObject.GetComponent<BulletController>();
      float damage = bc.GetDamage();

      float angle = Vector3.Angle(m_transform.forward, bc.gameObject.GetComponent<Transform>().forward);
      if (angle > 160 && angle < 200) damage *= 5;

      m_health -= damage;
      if (m_health < 0.0f)
      {
        ArenaManager.instance.StartCoroutine(ArenaManager.instance.RespawnCompetitor(m_entityID));
      }
    }
  }
}
