using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
  Transform p_transform;
  Transform m_transform;

  InputManager m_input;

  [SerializeField]
  float angleLimit;

  public float rotateSpeed = 200;
  float nextShoot = 0;

  void Awake()
  {
    m_transform = gameObject.GetComponent<Transform>();
    p_transform = m_transform.parent;

    m_input = p_transform.gameObject.GetComponent<InputManager>();
  }

  void Start()
  {

  }

  void Update()
  {
    updateMovement();

    if (m_input && (m_input.leftMouseButton || m_input.rightTrigger == 1))
    {
      Shoot();
    }
  }

  void updateMovement()
  {
    m_transform.LookAt(Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 10.0f)));
  }

  void Shoot()
  {
    if (Time.time > nextShoot)
    {
      WeaponContainer weapon = m_transform.GetChild(0).gameObject.GetComponent<WeaponContainer>();
      ParticleSystem particle = weapon.particlesystem.GetComponent<ParticleSystem>();

      nextShoot = Time.time + weapon.rateOfFire;

      GameObject bullet = Instantiate(weapon.bulletPrefab, weapon.bulletSpawn.position, weapon.bulletSpawn.rotation);
      weapon.initBullet(bullet, p_transform.gameObject.GetComponent<PlayerController>().m_entityID);

      particle.Play();
    }
  }
}
