using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : EntityController
{
  public AreaID m_currentArea { get; private set; }
  public EntityID m_targetID;
  public EnemyData m_data;
  public EnemyState m_state;
  public StateInformation m_stateInfo;

  public bool m_targetInSight;
  public bool m_grounded;

  public Rigidbody m_rigidbody;
  public Transform m_transform;
  public EnemyWeaponController m_weapon;
  public NavMeshAgent m_agent;

  public List<Transform> m_opponents;
  public Transform m_targetTransform;
  Vector3 m_targetLastKnownPosition;
  float m_targetTimeSinceSeen;

  float nextPathTime;
  public float nextPathWait = 1.0f;

  public qAction m_currentAction;
  public qAction m_targetAction;
  List<Action> m_combatActions;
  public List<qAction> m_pastActions;
  public CombatAction m_currentActionType;
  public float m_actionPenalty;
  public float m_lastRewardTime;
  public float m_currentActionValue;

  [SerializeField]
  public float m_viewDistance = 15.0f;
  public float m_viewAngle = 240.0f;
  [SerializeField]
  float m_lookSpeed = 7.0f;

  public int id;
  public int totalDeaths;
  public int idealAreaID;
  public float m_lastCombatAction = 0;

  public Brain m_brain;

  void Awake()
  {
    m_rigidbody = GetComponent<Rigidbody>();
    m_transform = GetComponent<Transform>();
    m_agent = GetComponent<NavMeshAgent>();
    m_brain = GetComponent<Brain>();
    m_weapon = m_transform.GetChild(0).gameObject.GetComponent<EnemyWeaponController>();
    m_spawnPosition = m_transform.position;
    m_stateInfo = new StateInformation();

    id = (int)m_entityID;

    m_data = Serializer.Load<EnemyData>(Application.dataPath + "/EnemyData" + id.ToString() + ".ed");
    if (m_data == null) m_data = new EnemyData();

    m_currentActionType = CombatAction.empty;
    m_lastRewardTime = 0;
  }

  void Start()
  {
    m_targetID = EntityID.empty;
    m_opponents = ArenaManager.instance.GetOpponents(m_entityID);

    m_currentAction = null;

    m_combatActions = new List<Action>();
    m_pastActions = new List<qAction>();

    m_combatActions.Add(NoAction);
    m_combatActions.Add(StrafeLeftAction);
    m_combatActions.Add(StrafeRightAction);
    m_combatActions.Add(MoveTowardAction);
    m_combatActions.Add(MoveAwayAction);
  }

  public void Respawn()
  {
    m_targetID = (int)EntityID.empty;
    m_targetTransform = null;
  }

  void Update()
  {
    id = (int)m_entityID;

    UpdateStateInfo();

    TargetUpdate();

    CombatUpdate();

    if (m_currentActionType != CombatAction.empty && m_currentAction != null && m_targetID != EntityID.empty)
    {
      if (m_currentAction.action_ != null) m_currentAction.action_();

      if (m_currentAction.GetExpired())
      {
        m_pastActions.Add(m_currentAction);
        TrimPastActions();

        m_currentAction = null;
        m_currentActionType = CombatAction.empty;
      }
    }

    if (!m_agent.pathPending && m_agent.remainingDistance < 1.0f)
    {
      GetNewPath();
    }
  }

  void GetNewPath()
  {
    if (m_targetID == EntityID.empty)
    {
      idealAreaID = m_brain.GetIdealArea(m_data.GetAreaData());

      int attempts = 0;
      while(!m_agent.pathPending && attempts < 10)
      {
        Transform areaTransform = ArenaManager.instance.m_areas[idealAreaID];
        Collider areaCollider = areaTransform.gameObject.GetComponent<Collider>();

        float x = UnityEngine.Random.Range(areaCollider.bounds.min.x, areaCollider.bounds.max.x);
        float z = UnityEngine.Random.Range(areaCollider.bounds.min.z, areaCollider.bounds.max.z);

        NavMeshHit hit;
        if (NavMesh.SamplePosition(new Vector3(x, 0, z), out hit, 5.0f, NavMesh.AllAreas))
        {
          m_agent.destination = hit.position;
        }

        attempts++;
      }
    }
    else if (TargetInSight())
    {
      int attempts = 0;
      while(!m_agent.pathPending && attempts < 10)
      {
        Vector3 destination = m_transform.position + (UnityEngine.Random.insideUnitSphere * 3);

        NavMeshHit hit;
        if (NavMesh.SamplePosition(new Vector3(destination.x, 0, destination.z), out hit, 5.0f, NavMesh.AllAreas))
        {
          m_agent.destination = hit.position;
        }

        attempts++;
      }
    }
  }

  void GetPathNearLastSeen()
  {
    int attempts = 0;

    while(!m_agent.pathPending && attempts < 10)
    {
      float x = UnityEngine.Random.Range(m_targetLastKnownPosition.x - 2, m_targetLastKnownPosition.x + 2);
      float z = UnityEngine.Random.Range(m_targetLastKnownPosition.z - 2, m_targetLastKnownPosition.z + 2);

      NavMeshHit hit;
      if (NavMesh.SamplePosition(new Vector3(x, 0, z), out hit, 5.0f, NavMesh.AllAreas))
      {
        m_agent.destination = hit.position;
      }
      attempts++;
    }
  }

  bool OpponentInSight()
  {
    if (m_stateInfo.opp1InView) return true;
    if (m_stateInfo.opp2InView) return true;
    if (m_stateInfo.opp3InView) return true;
    return false;
  }

  bool OpponentInSight(int id)
  {
    switch(id)
    {
      case 0:
        return m_stateInfo.opp1InView;
      break;
      case 1:
        return m_stateInfo.opp2InView;
      break;
      case 2:
        return m_stateInfo.opp3InView;
      break;
    }

    return false;
  }

  bool TargetInSight()
  {
    RaycastHit hit;

    Physics.Raycast(m_transform.position, (m_targetTransform.position - m_transform.position), out hit);

    if (hit.transform == m_targetTransform) return true;

    return false;
  }

  void TargetUpdate()
  {
    double[] values;
    int[] opponentIDs = m_brain.GetTargetingAction(GetTargetingState(), out values);

    int id = 2;
    bool targetAquired = false;

    while(!targetAquired && id >= 0)
    {
      if (OpponentInSight(opponentIDs[id]))
      {
        SetTarget(m_opponents[opponentIDs[id]].GetComponent<EntityController>().m_entityID);
        m_targetAction = new qAction(null, 0, 0, opponentIDs[id], (float)values[id], ActionType.movement, GetTargetingState());
        targetAquired = true;
      }
      id--;
    }

    if (!targetAquired) m_targetAction = new qAction(null, 0, 0, opponentIDs[2], (float)values[2], ActionType.movement, GetTargetingState());

    if (m_targetID != EntityID.empty && !TargetInSight())
    {
      if (m_targetTimeSinceSeen > 5) ResetTarget();

      GetPathNearLastSeen();
      m_weapon.shoot = false;
    }
    else if (m_targetID != EntityID.empty)
    {
      m_targetTimeSinceSeen = Time.time;
      m_targetLastKnownPosition = m_targetTransform.position;

      Vector3 targetDir = (m_targetTransform.position + (UnityEngine.Random.insideUnitSphere * 0.65f))- m_transform.position;
      Vector3 dir = Vector3.RotateTowards(m_transform.forward, targetDir, m_lookSpeed * Time.deltaTime, 0.0f);
      transform.rotation = Quaternion.LookRotation(dir);

      m_weapon.shoot = true;
    }
    else
    {
      m_currentActionType = CombatAction.empty;
      m_weapon.shoot = false;
    }
  }

  void CombatUpdate()
  {
    if (m_currentActionType == CombatAction.empty && m_targetID != EntityID.empty)
    {
      double predictedValue;
      CombatAction nextAction = m_brain.GetBestAction(GetCombatState(), out predictedValue);

      qAction newAction = new qAction();
      switch(nextAction)
      {
        case CombatAction.none:
          m_currentAction = new qAction(m_combatActions[(int)CombatAction.none], 0.5f, Time.time, (int)CombatAction.none, (float)predictedValue, ActionType.combat, GetCombatState());
        break;
        case CombatAction.strafeLeft:
          m_currentAction = new qAction(m_combatActions[(int)CombatAction.strafeLeft], 0.5f, Time.time, (int)CombatAction.strafeLeft, (float)predictedValue, ActionType.combat, GetCombatState());
        break;
        case CombatAction.strafeRight:
          m_currentAction = new qAction(m_combatActions[(int)CombatAction.strafeRight], 0.5f, Time.time, (int)CombatAction.strafeRight, (float)predictedValue, ActionType.combat, GetCombatState());
        break;
        case CombatAction.moveToward:
          m_currentAction = new qAction(m_combatActions[(int)CombatAction.moveToward], 0.5f, Time.time, (int)CombatAction.moveToward, (float)predictedValue, ActionType.combat, GetCombatState());
        break;
        case CombatAction.moveAway:
          m_currentAction = new qAction(m_combatActions[(int)CombatAction.moveAway], 0.5f, Time.time, (int)CombatAction.moveAway, (float)predictedValue, ActionType.combat, GetCombatState());
        break;
      }

      m_currentActionValue = (float)predictedValue;
      m_currentActionType = nextAction;
    }
  }

  double[] GetMovementState()
  {
    double[] state = new double[3];
    state[0] = m_transform.position.x;
    state[1] = m_transform.position.y;
    state[2] = m_transform.position.z;

    return state;
  }

  double[] GetTargetingState()
  {
    List<double> inputs = new List<double>();

    inputs.Add(m_stateInfo.opp1Distance);
    inputs.Add(m_stateInfo.opp1Angle);
    if (m_stateInfo.opp1InView) inputs.Add(1);
    else inputs.Add(0);
    inputs.Add(m_stateInfo.opp1PredHealth);

    inputs.Add(m_stateInfo.opp2Distance);
    inputs.Add(m_stateInfo.opp2Angle);
    if (m_stateInfo.opp2InView) inputs.Add(1);
    else inputs.Add(0);
    inputs.Add(m_stateInfo.opp2PredHealth);

    inputs.Add(m_stateInfo.opp3Distance);
    inputs.Add(m_stateInfo.opp3Angle);
    if (m_stateInfo.opp3InView) inputs.Add(1);
    else inputs.Add(0);
    inputs.Add(m_stateInfo.opp3PredHealth);

    return inputs.ToArray();
  }

  double[] GetCombatState()
  {
    if (m_targetID != EntityID.empty)
    {
      List<double> inputs = new List<double>();

      inputs.Add((m_transform.position - m_targetTransform.position).magnitude);
      double angle = Vector3.Angle(m_targetTransform.forward, (m_targetTransform.position - m_transform.position));
      inputs.Add(angle);
      if (TargetInSight()) inputs.Add(1.0);
      else inputs.Add(0.0);

      return inputs.ToArray();
    }

    return new double[0];
  }

  void UpdateStateInfo()
  {
    RaycastHit hit;

    m_stateInfo.opp1Distance = (m_transform.position - m_opponents[0].position).magnitude;
    m_stateInfo.opp1Angle = Vector3.Angle(m_transform.forward, (m_opponents[0].position - m_transform.position));
    Physics.Raycast(m_transform.position, (m_opponents[0].position - m_transform.position), out hit);
    if (hit.transform == m_opponents[0] && (m_transform.position - m_opponents[0].position).magnitude < m_viewDistance
    && m_stateInfo.opp1Angle < m_viewAngle) m_stateInfo.opp1InView = true;
    else m_stateInfo.opp1InView = false;
    m_stateInfo.opp1PredHealth = m_opponents[0].GetComponent<EntityController>().m_health;

    m_stateInfo.opp2Distance = (m_transform.position - m_opponents[1].position).magnitude;
    m_stateInfo.opp2Angle = Vector3.Angle(m_transform.forward, (m_opponents[1].position - m_transform.position));
    Physics.Raycast(m_transform.position, (m_opponents[1].position - m_transform.position), out hit);
    if (hit.transform == m_opponents[1] && (m_transform.position - m_opponents[1].position).magnitude < m_viewDistance
    && m_stateInfo.opp2Angle < m_viewAngle) m_stateInfo.opp2InView = true;
    else m_stateInfo.opp2InView = false;
    m_stateInfo.opp2PredHealth = m_opponents[1].GetComponent<EntityController>().m_health;

    m_stateInfo.opp3Distance = (m_transform.position - m_opponents[2].position).magnitude;
    m_stateInfo.opp3Angle = Vector3.Angle(m_transform.forward, (m_opponents[2].position - m_transform.position));
    Physics.Raycast(m_transform.position, (m_opponents[2].position - m_transform.position), out hit);
    if (hit.transform == m_opponents[2] && (m_transform.position - m_opponents[2].position).magnitude < m_viewDistance
    && m_stateInfo.opp3Angle < m_viewAngle) m_stateInfo.opp3InView = true;
    else m_stateInfo.opp3InView = false;
    m_stateInfo.opp3PredHealth = m_opponents[2].GetComponent<EntityController>().m_health;

    m_stateInfo.health = m_health;

    m_stateInfo.currentArea = (int)m_currentArea;
  }

  void LateUpdate()
  {
    m_transform.localEulerAngles = new Vector3(0.0f, m_transform.localEulerAngles.y, 0.0f);

  }

  void FixedUpdate()
  {
    m_rigidbody.velocity = new Vector3(Mathf.Clamp(m_rigidbody.velocity.x, -5.0f, 5.0f),
                                      Mathf.Clamp(m_rigidbody.velocity.y, -100.0f, 100.0f),
                                      Mathf.Clamp(m_rigidbody.velocity.z, -5.0f, 5.0f));
  }

  void ShootWeapon()
  {
    Vector3 direction = m_targetTransform.position - m_transform.position;
    float angle = Vector3.Angle(m_transform.forward, direction);
    if (angle < 30) m_weapon.shoot = true;
    else m_weapon.shoot = false;
  }

  public void ResetTarget()
  {
    m_targetID = EntityID.empty;
    m_targetTransform = null;
    m_targetInSight = false;
    if (m_agent.pathPending) m_agent.ResetPath();
  }

  void SetTarget(EntityID entity)
  {
    ResetTarget();
    m_targetID = entity;
    m_targetTransform = ArenaManager.instance.GetCompetitorTransform(entity);
    if (m_agent.pathPending) m_agent.ResetPath();
  }

  void OnTriggerEnter(Collider other)
  {
    AreaContainer area;
    if (area = other.gameObject.GetComponent<AreaContainer>())
    {
      m_currentArea = area.m_ID;
    }

    if (other.gameObject.tag == "WalkableGround")
    {
      m_grounded = true;
    }
  }

  void OnCollisionEnter(Collision collision)
  {
    if (collision.gameObject.tag == "Bullet")
    {
      BulletController bc = collision.gameObject.GetComponent<BulletController>();
      float damage = bc.GetDamage();

      SetTarget(bc.m_owner);

      float angle = Vector3.Angle(m_transform.forward, bc.gameObject.GetComponent<Transform>().forward);
      if (angle > 160 && angle < 200) damage *= 5;

      m_health -= damage;
      if (m_health < 0.0f)
      {
        ArenaManager.instance.CompetitorKilled(bc.m_owner, m_entityID);
        ArenaManager.instance.StartCoroutine(ArenaManager.instance.RespawnCompetitor(m_entityID));
      }
    }
  }

  void OnTriggerLeave(Collider other)
  {
    if (other.gameObject.tag == "WalkableGround")
      m_grounded = false;
  }

  public void GotAKill()
  {
    m_data.totalKills++;

    switch (m_currentArea)
    {
      case AreaID.area1:
      m_data.area1Kill++;
      break;
      case AreaID.area2:
      m_data.area2Kill++;
      break;
      case AreaID.area3:
      m_data.area3Kill++;
      break;
      case AreaID.area4:
      m_data.area4Kill++;
      break;
      case AreaID.area5:
      m_data.area5Kill++;
      break;
    }

    ApplyTargetingReward(1);

    ResetTarget();

    ApplyCombatReward(2);
  }

  public void Died(EntityID killer)
  {
    m_data.totalDeaths++;
    totalDeaths++;

    switch (m_currentArea)
    {
      case AreaID.area1:
      m_data.area1Death++;
      break;
      case AreaID.area2:
      m_data.area2Death++;
      break;
      case AreaID.area3:
      m_data.area3Death++;
      break;
      case AreaID.area4:
      m_data.area4Death++;
      break;
      case AreaID.area5:
      m_data.area5Death++;
      break;
    }

    if (m_targetID == killer) ApplyTargetingReward(1);
    else ApplyTargetingReward(-1);

    ApplyCombatReward(-2);

    m_currentActionType = CombatAction.empty;
  }

  void OnDestroy()
  {
    Serializer.Save<EnemyData>(Application.dataPath + "/EnemyData" + id.ToString() + ".ed", m_data);
  }

  void NoAction()
  {

  }

  void StrafeLeftAction()
  {
    Vector3 force = m_transform.right * 100;
    m_rigidbody.AddForce(-force);
  }

  void StrafeRightAction()
  {
    Vector3 force = m_transform.right * 100;
    m_rigidbody.AddForce(force);
  }

  void MoveTowardAction()
  {
    Vector3 force = (m_targetTransform.position - m_transform.position).normalized * 100;
    m_rigidbody.AddForce(force);
  }

  void MoveAwayAction()
  {
    Vector3 force = (m_targetTransform.position - m_transform.position).normalized * 100;
    m_rigidbody.AddForce(-force);
  }

  float CalculateCombatReward()
  {
    return 1;
  }

  void TrimPastActions()
  {
    if (m_pastActions.Count > 5)
    {
      m_pastActions.RemoveAt(0);
    }
  }

  void ApplyCombatReward(float reward)
  {
    float rewardCache = reward;

    for (int i = m_pastActions.Count - 1; i > 0; i--)
    {
      m_pastActions[i].SetReward(rewardCache);
      rewardCache *= m_actionPenalty;
    }

    m_brain.CombatLearn(m_pastActions);

    m_lastRewardTime = Time.time;
    m_pastActions = new List<qAction>();
  }

  void ApplyTargetingReward(float reward)
  {
    m_targetAction.SetReward(reward);

    m_brain.TargetLearn(m_targetAction);
  }
}

[System.Serializable]
public class EnemyData
{
  public int totalDeaths;
  public int totalKills;
  public float area1Kill;
  public float area2Kill;
  public float area3Kill;
  public float area4Kill;
  public float area5Kill;
  public float area1Death;
  public float area2Death;
  public float area3Death;
  public float area4Death;
  public float area5Death;

  public double[] GetAreaData()
  {
    List<Double> data = new List<Double>();

    data.Add(area1Kill);
    data.Add(area1Death);

    data.Add(area2Kill);
    data.Add(area2Death);

    data.Add(area3Kill);
    data.Add(area3Death);

    data.Add(area4Kill);
    data.Add(area4Death);

    data.Add(area5Kill);
    data.Add(area5Death);

    return data.ToArray();
  }
}

public class StateInformation
{
  public float opp1Distance;
  public float opp1Angle;
  public bool opp1InView;
  public float opp1PredHealth;
  public float opp2Distance;
  public float opp2Angle;
  public bool opp2InView;
  public float opp2PredHealth;
  public float opp3Distance;
  public float opp3Angle;
  public bool opp3InView;
  public float opp3PredHealth;
  public float health;
  public int currentArea;
}
