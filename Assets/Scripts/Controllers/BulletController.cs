using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class BulletController : MonoBehaviour
{
  public float speed;
  public float range;
  public float damage;
  public GameObject effectPrefab;
  public EntityID m_owner;

  float distance;

  Transform m_transform;
  Vector3 startPosition;

  void Awake()
  {
    m_transform = gameObject.GetComponent<Transform>();
    startPosition = m_transform.position;
  }

  void Update()
  {
    distance = (m_transform.position - startPosition).magnitude;

    m_transform.Translate(Vector3.forward * (speed * Time.deltaTime));

    if (distance > range)
    {
      Destroy(gameObject);
    }
  }

  public float GetDamage()
  {
    float percentage = 1 - (distance / range);
    percentage = Mathf.Clamp(percentage, 0.4f, 1.0f);

    return damage * percentage;
  }

  void OnCollisionEnter(Collision collision)
  {
    Instantiate(effectPrefab, m_transform.position, Quaternion.identity);

    Destroy(gameObject);
  }
}
