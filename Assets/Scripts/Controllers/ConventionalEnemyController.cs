using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.AI;

public class ConventionalEnemyController : EntityController
{
  public EntityID m_targetID;

  public Rigidbody m_rigidbody;
  public Transform m_transform;
  public EnemyWeaponController m_weapon;
  public NavMeshAgent m_agent;

  public List<Transform> m_opponents;
  public Transform m_targetTransform;
  public bool m_targetInSight;
  Vector3 m_targetLastKnownPosition;
  float m_targetTimeSinceSeen;

  public float m_viewDistance = 12.5f;
  public float m_viewAngle = 240.0f;

  void Awake()
  {
    m_rigidbody = GetComponent<Rigidbody>();
    m_transform = GetComponent<Transform>();
    m_agent = GetComponent<NavMeshAgent>();
    m_weapon = m_transform.GetChild(0).gameObject.GetComponent<EnemyWeaponController>();
    m_spawnPosition = m_transform.position;
  }

  void Start()
  {
    m_targetID = EntityID.empty;
    m_opponents = ArenaManager.instance.GetOpponents(m_entityID);
  }

  void Update()
  {
    TargetUpdate();

    if (!m_agent.pathPending && isOnNavMesh() && m_agent.remainingDistance < 1.0f)
    {
      GetNewPath();
    }
  }

  void LateUpdate()
  {
    m_transform.localEulerAngles = new Vector3(0.0f, m_transform.localEulerAngles.y, 0.0f);

  }

  void FixedUpdate()
  {
    m_rigidbody.velocity = new Vector3(Mathf.Clamp(m_rigidbody.velocity.x, -5.0f, 5.0f),
                                      Mathf.Clamp(m_rigidbody.velocity.y, -100.0f, 100.0f),
                                      Mathf.Clamp(m_rigidbody.velocity.z, -5.0f, 5.0f));
  }

  void TargetUpdate()
  {
    if (m_targetID == EntityID.empty)
    {
      m_targetID = GetTarget();
      if (m_targetID != EntityID.empty) m_targetTransform = ArenaManager.instance.GetCompetitorTransform(m_targetID);

      m_weapon.shoot = false;
    }
    else if (TargetInSight())
    {
      m_transform.LookAt(m_targetTransform);

      m_weapon.shoot = true;

      m_targetTimeSinceSeen = Time.time;
      m_targetLastKnownPosition = m_targetTransform.position;
    }
    else
    {
      if (Time.time - m_targetTimeSinceSeen > 5) ResetTarget();
    }
  }

  bool TargetInSight()
  {
    RaycastHit hit;

    Physics.Raycast(m_transform.position, (m_targetTransform.position - m_transform.position), out hit);
    if (hit.transform == m_targetTransform && (m_transform.position - m_targetTransform.position).magnitude < m_viewDistance)
      return m_targetInSight = true;

    return m_targetInSight = false;
  }

  EntityID GetTarget()
  {
    RaycastHit hit;
    List<EntityID> enemiesInSight = new List<EntityID>();

    Physics.Raycast(m_transform.position, (m_opponents[0].position - m_transform.position), out hit);
    if (hit.transform == m_opponents[0] && (m_transform.position - m_opponents[0].position).magnitude < m_viewDistance)
      enemiesInSight.Add(m_opponents[0].GetComponent<EntityController>().m_entityID);

    Physics.Raycast(m_transform.position, (m_opponents[1].position - m_transform.position), out hit);
    if (hit.transform == m_opponents[1] && (m_transform.position - m_opponents[1].position).magnitude < m_viewDistance)
      enemiesInSight.Add(m_opponents[1].GetComponent<EntityController>().m_entityID);

    Physics.Raycast(m_transform.position, (m_opponents[2].position - m_transform.position), out hit);
    if (hit.transform == m_opponents[2] && (m_transform.position - m_opponents[2].position).magnitude < m_viewDistance)
      enemiesInSight.Add(m_opponents[2].GetComponent<EntityController>().m_entityID);

    if (enemiesInSight.Count > 0)
    {
      ResetTarget();
      return enemiesInSight[UnityEngine.Random.Range(0, enemiesInSight.Count)];
    }
    else
      return EntityID.empty;
  }

  public void ResetTarget()
  {
    m_targetID = EntityID.empty;
    m_targetTransform = null;
    m_targetInSight = false;
    if (m_agent.pathPending) m_agent.ResetPath();
  }

  public void Respawn()
  {
    ResetTarget();
  }

  void GetNewPath()
  {
    int attempts = 0;
    if (m_targetID == EntityID.empty)
    {
      while(!m_agent.pathPending && attempts < 10)
      {
        Transform areaTransform = ArenaManager.instance.m_areas[0];
        Collider areaCollider = areaTransform.gameObject.GetComponent<Collider>();

        float x = UnityEngine.Random.Range(areaCollider.bounds.min.x, areaCollider.bounds.max.x);
        float z = UnityEngine.Random.Range(areaCollider.bounds.min.z, areaCollider.bounds.max.z);

        NavMeshHit hit;
        if (NavMesh.SamplePosition(new Vector3(x, 0, z), out hit, 5.0f, NavMesh.AllAreas));
          m_agent.destination = hit.position;
      }

      attempts++;
    }
    else if (m_targetInSight)
    {
      while (!m_agent.pathPending && attempts < 10)
      {
        Vector3 destination = m_targetTransform.position + (UnityEngine.Random.insideUnitSphere * 5);

        NavMeshHit hit;
        if (NavMesh.SamplePosition(new Vector3(destination.x, 0, destination.z), out hit, 5.0f, NavMesh.AllAreas))
          m_agent.destination = hit.position;

        attempts++;
      }
    }
    else
    {
      m_agent.destination = m_targetLastKnownPosition;

    }
  }

  void SetTarget(EntityID entity)
  {
    ResetTarget();
    m_targetID = entity;
    m_targetTransform = ArenaManager.instance.GetCompetitorTransform(entity);
    if (m_agent.pathPending) m_agent.ResetPath();
  }

  void OnCollisionEnter(Collision collision)
  {
    if (collision.gameObject.tag == "Bullet")
    {
      BulletController bc = collision.gameObject.GetComponent<BulletController>();
      float damage = bc.GetDamage();

      SetTarget(bc.m_owner);

      float angle = Vector3.Angle(m_transform.forward, bc.gameObject.GetComponent<Transform>().forward);
      if (angle > 160 && angle < 200) damage *= 5;

      m_health -= damage;
      if (m_health < 0.0f)
      {
        ArenaManager.instance.StartCoroutine(ArenaManager.instance.RespawnCompetitor(m_entityID));
      }
    }
  }

  bool isOnNavMesh()
  {
    NavMeshHit hit;

    if (NavMesh.SamplePosition(m_transform.position, out hit, 2.0f, NavMesh.AllAreas))
    {
      if (Mathf.Approximately(m_transform.position.x, hit.position.x) && Mathf.Approximately(m_transform.position.z, hit.position.z))
      {
        return m_transform.position.y >= hit.position.y;
      }
    }

    return false;
  }
}
