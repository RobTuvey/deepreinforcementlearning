using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.AI;

[System.Serializable]
public class qAction
{
  public Action action_;
  public Action rewardCalculator_;
  public ActionType actionType_;
  public double[] state_;

  int id_;
  float duration_;
  float start_;
  float reward_;
  float predValue_;

  public qAction()
  {

  }

  public qAction(Action action, float duration, float start, int id, float predValue, ActionType actionType, double[] state)
  {
    action_ = action;
    duration_ = duration;
    start_ = start;
    id_ = id;
    actionType_ = actionType;
    reward_ = 0;
    state_ = state;
    predValue_ = predValue;
  }

  public bool GetExpired()
  {
    float time = Time.time;

    if (time - start_ > duration_)
      return true;

    return false;
  }

  public void SetReward(float reward)
  {
    reward_ = reward;
  }

  public Memory CreateMemory()
  {
    return new Memory(state_, id_, reward_, predValue_, state_);
  }

  public int GetID()
  {
    return id_;
  }

  public float GetReward()
  {
    return reward_;
  }

  public float GetPredValue()
  {
    return predValue_;
  }
}
