using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    PlayerController m_playerController;

    public float mov_xAxis;
    public float mov_zAxis;
    public float loo_xAxis;
    public float loo_yAxis;
    public float rightTrigger;
    public float leftTrigger;

    public bool button0;
    public bool leftMouseButton;

    public bool m_allowInput = true;

    void Awake()
    {
      m_playerController = GetComponent<PlayerController>();

      Screen.lockCursor = true;
      Cursor.visible = false;
    }

    void Start()
    {

    }

    void Update()
    {
      if (m_allowInput)
      {
        mov_xAxis = Input.GetAxisRaw("MoveHorizontal");
        if (Input.GetKey(KeyCode.D)) mov_xAxis += 0.85f;
        if (Input.GetKey(KeyCode.A)) mov_xAxis -= 0.85f;

        mov_zAxis = Input.GetAxisRaw("MoveVertical");
        if (Input.GetKey(KeyCode.W)) mov_zAxis += 0.85f;
        if (Input.GetKey(KeyCode.S)) mov_zAxis -= 0.85f;

        loo_xAxis = Input.GetAxisRaw("LookHorizontal");
        loo_xAxis += Input.GetAxisRaw("MouseHorizontal");
        loo_yAxis = Input.GetAxisRaw("LookVertical");
        loo_yAxis += Input.GetAxisRaw("MouseVertical");
        rightTrigger = Input.GetAxisRaw("RightTrigger");
        leftTrigger = Input.GetAxisRaw("LeftTrigger");

        button0 = Input.GetKey("joystick button 0");
        leftMouseButton = Input.GetMouseButton(0);
      }
    }

    void OnRoundStart()
    {
      m_allowInput = true;
    }
}
