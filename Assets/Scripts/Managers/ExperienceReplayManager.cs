using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ExperienceReplayManager : MonoBehaviour
{

  public List<ExperienceReplayController> m_agents;

  void Update()
  {
    if (Input.GetKey(KeyCode.Escape))
      SceneManager.LoadScene("menu");
  }

  public void StartReplay()
  {
    foreach(ExperienceReplayController agent in m_agents)
    {
      agent.StartLearning();
    }
  }

  public void StopReplay()
  {
    foreach (ExperienceReplayController agent in m_agents)
    {
      agent.StopLearning();
    }
  }

  public void ImportAgentMemories()
  {
    foreach (ExperienceReplayController agent in m_agents)
    {
      agent.ImportMemories();
    }
  }

  public List<Memory> ImportMemories(string directory)
  {
    List<Memory> memories = new List<Memory>();

    DirectoryInfo memoryDirectoryInfo = new DirectoryInfo(directory);
    FileInfo[] fileInfo = memoryDirectoryInfo.GetFiles("*.*", SearchOption.AllDirectories);

    foreach (FileInfo file in fileInfo)
    {
      if (file.Extension == ".mem")
      {
        List<Memory> newMemories = Serializer.Load<List<Memory>>(file.FullName);
        memories.AddRange(newMemories);
      }
    }

    return memories;
  }

}
