using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
  public void LoadVSRL()
  {
    SceneManager.LoadScene("playerVsRL", LoadSceneMode.Single);
  }

  public void LoadVSNormal()
  {
    SceneManager.LoadScene("playerVsNormal", LoadSceneMode.Single);
  }

  public void LoadRLArena()
  {
    SceneManager.LoadScene("RLArena", LoadSceneMode.Single);
  }

  public void LoadExperienceReplay()
  {
    SceneManager.LoadScene("experienceReplay", LoadSceneMode.Single);
  }
}
