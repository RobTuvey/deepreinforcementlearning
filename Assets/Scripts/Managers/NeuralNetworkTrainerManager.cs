using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;
using UnityEngine.UI;
using NeuralNetwork;

public class NeuralNetworkTrainerManager : MonoBehaviour
{
  public GameObject m_newNetworkDialoguePrefab;
  public GameObject m_newNetworkDialogue;

  public GameObject m_timeLabel;
  public GameObject m_errorLabel;
  public GameObject m_epochLabel;
  public GameObject m_stopButton;
  public GameObject m_directoryInputField;
  public GameObject m_functionFeedback;

  public GameObject m_networkNameLabel;
  public GameObject m_inputSizeLabel;
  public GameObject m_hiddenSizeLabel;
  public GameObject m_hiddenLayersLabel;
  public GameObject m_outputSizeLabel;
  public GameObject m_learningRateLabel;
  public GameObject m_momentumLabel;

  public bool m_train;

  public string m_networkFilename;
  public string m_networkOutputDirectory;

  public double m_error;
  public double m_epoch;
  public double m_time;

  NeuralNet m_neuralNetwork;
  List<DataSet> m_trainingData;

  void Awake()
  {
    m_timeLabel.GetComponent<Text>().text = "";
    m_errorLabel.GetComponent<Text>().text = "";
    m_epochLabel.GetComponent<Text>().text = "";
    m_functionFeedback.GetComponent<Text>().text = "";

    m_networkNameLabel.GetComponent<Text>().text = "";
    m_inputSizeLabel.GetComponent<Text>().text = "";
    m_hiddenSizeLabel.GetComponent<Text>().text = "";
    m_hiddenLayersLabel.GetComponent<Text>().text = "";
    m_outputSizeLabel.GetComponent<Text>().text = "";
    m_learningRateLabel.GetComponent<Text>().text = "";
    m_momentumLabel.GetComponent<Text>().text = "";

    Application.runInBackground = true;
  }

  void Start()
  {

  }

  void Update()
  {
    if (m_train)
      m_stopButton.active = true;
    else
      m_stopButton.active = false;
  }

  void FixedUpdate()
  {
    if (m_neuralNetwork != null)
    {
      if (m_train)
      {
        m_time += Time.deltaTime;
        m_timeLabel.GetComponent<Text>().text = System.Math.Round(m_time, 2).ToString();
      }
    }
  }

  public void OpenNewNetworkDialogue()
  {
    m_newNetworkDialogue = Instantiate(m_newNetworkDialoguePrefab, m_newNetworkDialoguePrefab.transform.position, Quaternion.identity);
    m_newNetworkDialogue.transform.parent = this.transform;
    m_newNetworkDialogue.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
  }

  public void CreateNewNetwork(int inputSize, int hiddenSize, int hiddenLayers, int outputSize, double learningRate, double momentum, string filename, string outputDirectory)
  {
    m_neuralNetwork = new NeuralNet(inputSize, hiddenSize, outputSize, hiddenLayers, learningRate, momentum);

    m_networkFilename = filename;
    if (outputDirectory.Length!=0)
      m_networkOutputDirectory = outputDirectory;
    else
      m_networkOutputDirectory = Application.dataPath + "/";

    m_timeLabel.GetComponent<Text>().text = "0";
    m_errorLabel.GetComponent<Text>().text = "0";
    m_epochLabel.GetComponent<Text>().text = "0";

    m_networkNameLabel.GetComponent<Text>().text = filename.ToString();
    m_inputSizeLabel.GetComponent<Text>().text = inputSize.ToString();
    m_hiddenSizeLabel.GetComponent<Text>().text = hiddenSize.ToString();
    m_hiddenLayersLabel.GetComponent<Text>().text = hiddenLayers.ToString();
    m_outputSizeLabel.GetComponent<Text>().text = outputSize.ToString();
    m_learningRateLabel.GetComponent<Text>().text = learningRate.ToString();
    m_momentumLabel.GetComponent<Text>().text = momentum.ToString();

    m_epoch = 0;
    m_error = 0;

    Destroy(m_newNetworkDialogue);
  }

  public void CancelNewNetwork()
  {
    Destroy(m_newNetworkDialogue);
  }

  public void LoadTrainingData()
  {
    string path = Application.dataPath + "/" + m_directoryInputField.GetComponent<InputField>().text;
    if (path.Length == 0)
    {
      m_functionFeedback.GetComponent<Text>().text = "No path specified.";
    }
    else
    {
      m_trainingData = Serializer.Load<List<DataSet>>(path);
      if (m_trainingData == null)
      {
        m_functionFeedback.GetComponent<Text>().text = "Specified file not found.";
      }
      else
      {
        m_functionFeedback.GetComponent<Text>().text = "Training data loaded.";
      }
    }

    m_directoryInputField.GetComponent<InputField>().text = "";
  }

  public void LoadNetwork()
  {
    string path = Application.dataPath + "/" + m_directoryInputField.GetComponent<InputField>().text;
    if (path.Length == 0)
    {
      m_functionFeedback.GetComponent<Text>().text = "No path specified.";
    }
    else
    {
      m_neuralNetwork = Serializer.Load<NeuralNet>(path);
      if (m_neuralNetwork == null)
      {
        m_functionFeedback.GetComponent<Text>().text = "Specified file not found.";
      }
      else
      {
        m_functionFeedback.GetComponent<Text>().text = "Neural network loaded.";

        m_networkNameLabel.GetComponent<Text>().text = m_directoryInputField.GetComponent<InputField>().text;
        m_inputSizeLabel.GetComponent<Text>().text = m_neuralNetwork.inputLayer_.Count.ToString();
        m_hiddenSizeLabel.GetComponent<Text>().text = m_neuralNetwork.hiddenLayers_[0].Count.ToString();
        m_hiddenLayersLabel.GetComponent<Text>().text = m_neuralNetwork.hiddenLayers_.Count.ToString();
        m_outputSizeLabel.GetComponent<Text>().text = m_neuralNetwork.outputLayer_.Count.ToString();
        m_learningRateLabel.GetComponent<Text>().text = m_neuralNetwork.learnRate_.ToString();
        m_momentumLabel.GetComponent<Text>().text = m_neuralNetwork.momentum_.ToString();
      }
    }

    m_directoryInputField.GetComponent<InputField>().text = "";
  }

  public void TrainNetwork()
  {
    m_error = 1.0;
    m_train = true;
    StartCoroutine(Train());
  }

  public void SaveNetwork()
  {
    if (!m_train)
    {
      Serializer.Save<NeuralNet>(m_networkOutputDirectory + m_networkFilename, m_neuralNetwork);
      m_functionFeedback.GetComponent<Text>().text = "Neural network saved.";
    }
  }

  IEnumerator Train()
  {
    double minimumError = 0.01;

    while (m_error > minimumError && m_epoch < int.MaxValue && m_train)
    {
      List<double> errors = new List<double>();
      foreach(DataSet dataSet in m_trainingData)
      {
        m_neuralNetwork.ForwardPropogate(dataSet.values_);
        m_neuralNetwork.BackPropogate(dataSet.targets_);
        errors.Add(m_neuralNetwork.CalculateError(dataSet.targets_));
      }

      m_error = errors.Average();
      m_epoch++;

      m_errorLabel.GetComponent<Text>().text = System.Math.Round(m_error, 5).ToString();
      m_epochLabel.GetComponent<Text>().text = m_epoch.ToString();

      yield return null;
    }

    m_train = false;
  }

  public void StopTraining()
  {
    m_train = false;
  }
}
