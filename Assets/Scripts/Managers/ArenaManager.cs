using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class ArenaManager : MonoBehaviour
{
  public static ArenaManager instance { get; private set; }

  public Transform[] m_areas;

  public List<EntityController> m_competitors;

  public GameObject[] m_competitorPrefabs;

  public float m_respawnDelay;

  void Awake()
  {
    instance = this;

    Application.runInBackground = true;
  }

  void Update()
  {
    if (Input.GetKey(KeyCode.Escape))
      SceneManager.LoadScene("menu");
  }

  public Transform GetCompetitorTransform(EntityID competitor)
  {
    foreach (EntityController entity in m_competitors)
    {
      if (entity.m_entityID == competitor)
        return entity.gameObject.GetComponent<Transform>();
    }

    return null;
  }

  public List<Transform> GetOpponents(EntityID entity)
  {
    List<Transform> m_opponents = new List<Transform>();
    foreach(EntityController entityController in m_competitors)
    {
      if (entityController.m_entityID != entity)
        m_opponents.Add(entityController.gameObject.GetComponent<Transform>());
    }

    return m_opponents;
  }

  public void CompetitorKilled(EntityID killer, EntityID killed)
  {
    foreach (EntityController entity in m_competitors)
    {
      EnemyController enemy = entity.gameObject.GetComponent<EnemyController>();
      if (enemy && enemy.m_entityID == killed)
      {
          enemy.ResetTarget();
          enemy.Died(killer);
      }
      if (enemy && enemy.m_entityID == killer)
        enemy.GotAKill();
      if (enemy && enemy.m_targetID == killed)
        enemy.ResetTarget();
    }
  }

  void RemoveCompetitor(EntityID entity)
  {
    for (int i = 0; i < m_competitors.Count; i++)
    {
      if (m_competitors[i].m_entityID == entity)
      {
        m_competitors.RemoveAt(i);
        i--;
      }
    }
  }

  public IEnumerator RespawnCompetitor(EntityID entity)
  {
    Transform entityTransform = GetCompetitorTransform(entity);

    Vector3 m_spawnPosition = entityTransform.gameObject.GetComponent<EntityController>().m_spawnPosition;

    if (entityTransform.gameObject.GetComponent<NavMeshAgent>()) entityTransform.gameObject.GetComponent<NavMeshAgent>().enabled = false;
    if (entityTransform.gameObject.GetComponent<EnemyController>()) entityTransform.gameObject.GetComponent<EnemyController>().enabled = false;
    if (entityTransform.gameObject.GetComponent<ConventionalEnemyController>()) entityTransform.gameObject.GetComponent<ConventionalEnemyController>().enabled = false;
    if (entityTransform.gameObject.GetComponent<InputManager>()) entityTransform.gameObject.GetComponent<InputManager>().m_allowInput = false;
    entityTransform.position = m_spawnPosition;

    yield return new WaitForSeconds(m_respawnDelay);

    if (entityTransform.gameObject.GetComponent<NavMeshAgent>()) entityTransform.gameObject.GetComponent<NavMeshAgent>().enabled = true;
    if (entityTransform.gameObject.GetComponent<EnemyController>()) entityTransform.gameObject.GetComponent<EnemyController>().enabled = true;
    if (entityTransform.gameObject.GetComponent<ConventionalEnemyController>()) entityTransform.gameObject.GetComponent<ConventionalEnemyController>().enabled = true;
    if (entityTransform.gameObject.GetComponent<InputManager>()) entityTransform.gameObject.GetComponent<InputManager>().m_allowInput = true;
    entityTransform.gameObject.GetComponent<EntityController>().m_health = 100.0f;

    EnemyController enemyController = entityTransform.gameObject.GetComponent<EnemyController>();
    if (enemyController) enemyController.Respawn();

    ConventionalEnemyController cEnemyController = entityTransform.gameObject.GetComponent<ConventionalEnemyController>();
    if (cEnemyController) cEnemyController.Respawn();
  }
}
