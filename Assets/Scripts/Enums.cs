using System;
using System.Linq;

[System.Serializable]
public enum AreaID
{
  area1 = 0,
  area2 = 1,
  area3 = 2,
  area4 = 3,
  area5 = 4,
}
[System.Serializable]
public enum EntityID
{
  empty = 0,
  player = 1,
  enemy1 = 2,
  enemy2 = 3,
  enemy3 = 4,
  enemy4 = 5,
}
[System.Serializable]
public enum EnemyState
{
  dead = 0,
  searching = 1,
  attacking = 2,
  underAttack = 3,
  fleeing = 4,
}

public enum ActionType
{
    movement = 0,
    combat = 1,
}

public enum CombatAction
{
  none = 0,
  strafeLeft = 1,
  strafeRight = 2,
  moveToward = 3,
  moveAway = 4,
  empty = 5,
}

public class Utilities
{
  public static double[] SoftMax(double[] inputs)
  {
    double[] inputExp = inputs.Select(Math.Exp).ToArray();

    double expSum = inputExp.Sum();

    double[] softmax = inputExp.Select(i=> i / expSum).ToArray();

    return softmax;
  }

  public static int GetMax(double[] inputs)
  {
    int id = 0;
    double max = 0;
    for (int i = 0; i < inputs.GetLength(0); i++)
    {
      if (inputs[i] > max)
      {
        max = inputs[i];
        id = i;
      }
    }

    return id;
  }
}
