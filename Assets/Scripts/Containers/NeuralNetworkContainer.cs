using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using NeuralNetwork;

public class NeuralNetworkContainer : MonoBehaviour
{
  public NeuralNet m_neuralNet;

  [SerializeField]
  int inputSize = 10;
  [SerializeField]
  int hiddenSize = 5;
  [SerializeField]
  int outputSize = 5;
  [SerializeField]
  int hiddenLayers = 2;
  [SerializeField]
  double learnRate_ = 0.001;
  [SerializeField]
  double momentum_ = 0.5;

  public List<DataSet> data;

  void Awake()
  {
    data = new List<DataSet>();
    m_neuralNet = new NeuralNet(inputSize, hiddenSize, outputSize, hiddenLayers, learnRate_, momentum_);
  }

  public void AddData(double[] values, double[] targets)
  {
    data.Add(new DataSet(values, targets));
  }

  public void TrainNeuralNet()
  {
    m_neuralNet.Train(data, 0.01);
  }

  public double[] ComputeOutputs(double[] inputs)
  {
    return m_neuralNet.Compute(inputs);
  }

  public void SetData(List<DataSet> dataset)
  {
    data = dataset;
  }
}
