using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class WeaponContainer : MonoBehaviour
{
  [SerializeField]
  public int magazineSize;
  [SerializeField]
  public float damage;
  [SerializeField]
  public float rateOfFire;
  [SerializeField]
  public float bulletSpeed;
  [SerializeField]
  public float range;

  public GameObject bulletPrefab;
  public GameObject bulletEffectPrefab;
  public Transform bulletSpawn;
  public GameObject particlesystem;

  public void initBullet(GameObject bullet, EntityID owner)
  {
    BulletController bc = bullet.AddComponent<BulletController>();
    bc.speed = bulletSpeed;
    bc.range = range;
    bc.damage = damage;
    bc.m_owner = owner;
    bc.effectPrefab = bulletEffectPrefab;
  }
}
