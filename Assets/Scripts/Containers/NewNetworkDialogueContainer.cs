using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class NewNetworkDialogueContainer : MonoBehaviour
{
  public GameObject m_inputSizeInput;
  public GameObject m_hiddenSizeInput;
  public GameObject m_hiddenLayersInput;
  public GameObject m_outputSizeInput;
  public GameObject m_learningRateInput;
  public GameObject m_momentumInput;
  public GameObject m_filenameInput;
  public GameObject m_outputDirectoryInput;

  public void Cancel()
  {
    Destroy(gameObject);
  }

  public void Save()
  {
    NeuralNetworkTrainerManager m_trainer = GameObject.Find("Canvas").GetComponent<NeuralNetworkTrainerManager>();

    int inputSize = 0;
    int.TryParse(m_inputSizeInput.GetComponent<InputField>().text, out inputSize);

    int hiddenSize = 0;
    int.TryParse(m_hiddenSizeInput.GetComponent<InputField>().text, out hiddenSize);

    int hiddenLayers = 0;
    int.TryParse(m_hiddenLayersInput.GetComponent<InputField>().text, out hiddenLayers);

    int outputSize = 0;
    int.TryParse(m_outputSizeInput.GetComponent<InputField>().text, out outputSize);

    double learningRate = 0;
    double.TryParse(m_learningRateInput.GetComponent<InputField>().text, out learningRate);

    double momentum = 0;
    double.TryParse(m_momentumInput.GetComponent<InputField>().text, out momentum);

    string filename = m_filenameInput.GetComponent<InputField>().text;
    string outputDirectory = m_outputDirectoryInput.GetComponent<InputField>().text;

    m_trainer.CreateNewNetwork(inputSize, hiddenSize, hiddenLayers, outputSize, learningRate, momentum, filename, outputDirectory);
  }
}
