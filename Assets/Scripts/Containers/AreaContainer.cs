using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class AreaContainer : MonoBehaviour
{
  public AreaID m_ID { get; private set; }

  void Start()
  {
    switch (name)
    {
      case "Area 1":
        m_ID = AreaID.area1;
      break;
      case "Area 2":
        m_ID = AreaID.area2;
      break;
      case "Area 3":
        m_ID = AreaID.area3;
      break;
      case "Area 4":
        m_ID = AreaID.area4;
      break;
      case "Area 5":
        m_ID = AreaID.area5;
      break;
    }
  }
}
